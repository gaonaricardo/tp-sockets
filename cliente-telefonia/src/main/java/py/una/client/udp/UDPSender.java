package py.una.client.udp;

import java.io.*;
import java.net.*;
import py.una.entidad.*;

public class UDPSender extends Thread{

    private UDPServer udpServer;

    public UDPSender(UDPServer udpServer){
        this.udpServer = udpServer;
    }

    public void run(){
        DatagramPacket packet;
        while(true){
            try {
                packet = this.udpServer.getPacketToSend();
                this.udpServer.getSocket().send(packet);
            } catch(IOException e) {
                System.out.println("Error al enviar el paquete: " + e.getMessage());
            }
        }
    }
}