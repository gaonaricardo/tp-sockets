package py.una.client.udp;

import java.net.*;

public class UDPReceiver extends Thread{

    private UDPServer udpServer;

    public UDPReceiver(UDPServer udpServer){
        this.udpServer = udpServer;
    }
    
    public void run() {
        
        byte[] receiveData = new byte[1024];
        try{
            while (true) {
                receiveData = new byte[1024];
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                System.out.println("Esperando paquetes...");
                this.udpServer.getSocket().receive(receivePacket);

                new PacketHandler(this.udpServer.getClient(), receivePacket).start();;
            }
        }catch(Exception e){
            e.printStackTrace();
            System.exit(1);
        }
    }
}