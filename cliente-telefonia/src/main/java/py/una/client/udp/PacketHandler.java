package py.una.client.udp;

import py.una.client.Client;
import py.una.entidad.*;
import py.una.client.*;

import java.io.IOException;
import java.net.*;

public class PacketHandler extends Thread {
    private Client cliente;
    private DatagramPacket packet;

    public PacketHandler(Client cliente, DatagramPacket packet) {
        this.cliente = cliente;
        this.packet = packet;
    }

    public void run() {
        /* Obtenemos los campos del paquete en una instancia de PacketData */
        PacketData packetData = PacketDataJSON.toObject(new String(this.packet.getData()));
        /* Obtenemos la operación */
        Operation operation = packetData.getOperacion();

        System.out.println("Un nuevo paquete ha llegado!");

        if(operation == Operation.CONFIRMAR_RECEPCION) {
           int userID = Integer.parseInt(packetData.getContenido());
           this.cliente.setUserId(userID);
           System.out.println("User Id: " + userID);
        } else if(operation == Operation.VER_CONECTADOS) {
            String userJson = packetData.getContenido();
            this.cliente.mostrarListaConectados(User.toUserList(userJson));
        } else if(operation == Operation.INICIAR_LLAMADA) {
            User userCalled = User.toObject(packetData.getContenido());
            try {
                this.cliente.iniciarLlamada(userCalled);
            } catch(IOException e) {
                System.out.println("Error al iniciar la llamada: " + e.getMessage());
            }
        } else if(operation == Operation.LLAMADA_CONTESTADA) {
            synchronized(this.cliente) {
                try {
                    this.cliente.notify();
                } catch(IllegalMonitorStateException e) {
                    System.out.println("(PacketHandler)Error en el notify(): " + e.getMessage());
                }
            }
        } else if(operation == Operation.CALL_NOTIFY) {
            User caller = User.toObject(packetData.getContenido());
            new InterfazNotificacion(this.cliente, caller).setVisible(true);
        }
    }
}
