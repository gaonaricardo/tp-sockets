package py.una.client.udp;

import java.io.IOException;
import java.net.*;
import java.util.LinkedList;
import py.una.client.Client;

import java.util.concurrent.Semaphore;


public class UDPServer{

    private DatagramSocket socket;

    private LinkedList<DatagramPacket> enviosPendientes;
    private Semaphore sem;
    
    private UDPSender sender;
    private UDPReceiver receiver;

    private Client cliente;
    

    public UDPServer(Client cliente){
        try{
            this.socket = new DatagramSocket();
        }catch(IOException e){
            e.printStackTrace();
        }
        this.cliente = cliente;
        this.enviosPendientes = new LinkedList<DatagramPacket>();
        this.sender = new UDPSender(this);
        this.receiver = new UDPReceiver(this);
        this.sem = new Semaphore(1);
    }

    public void initServer(){
        try{
            this.sender.start();
            this.receiver.start();
            System.out.println("Servidor UDP iniciado!");
            System.out.println("Puerto utilizado: " + this.socket.getLocalPort());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public UDPSender getSender(){
        return this.sender;
    }

    public UDPReceiver getReceiver(){
        return this.receiver;
    }

    public LinkedList<DatagramPacket> getBuzonSalida(){
        return this.enviosPendientes;
    }

    public void addPacketToSend(DatagramPacket packet){
        try {
            /* Nos aseguramos de que solo un hilo a la vez acceda */
            synchronized(this.enviosPendientes){
                this.enviosPendientes.add(packet);
                this.enviosPendientes.notify();
            }
        } catch(IllegalMonitorStateException e) {
            System.out.println("Error en el notify(): " + e.getMessage());
        }
    }

    public DatagramPacket getPacketToSend() {
        DatagramPacket packet = null;

        try {
            /* Nos aseguramos de que solo un hilo a la vez acceda */
            synchronized(this.enviosPendientes) {
                if(this.enviosPendientes.size() == 0)
                    this.enviosPendientes.wait();
                
                packet = this.enviosPendientes.removeLast();
            }
        } catch (InterruptedException e1) {
            System.out.println("Error en el wait(): " + e1.getMessage());
        }

        return packet;
    }

    public Semaphore getSem(){
        return this.sem;
    }

    public DatagramSocket getSocket(){
        return this.socket;
    }

    public Client getClient() {
        return this.cliente;
    }

}  
