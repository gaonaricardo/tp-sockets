package py.una.client;

import java.net.*;
import java.io.*;

public class FromServer extends Thread {
    private Socket socket;
    private Client cliente;

    public FromServer(Socket socket, Client cliente) {
        this.socket = socket;
        this.cliente = cliente;
    }

    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));

            String fromServer = "";

            while((fromServer = in.readLine()) != null) {
                this.cliente.agregarNuevoMensaje(fromServer);
                System.out.println(fromServer);
            }
        } catch(IOException e) {
            System.out.println("Error al abrir flujo de entrada: " + e.getMessage());
            this.cliente.cerrarInterfaz();
        } /*catch(SocketException e) {
            System.out.println("Se cerro el flujo de entrada!");
        }*/
    }
}
