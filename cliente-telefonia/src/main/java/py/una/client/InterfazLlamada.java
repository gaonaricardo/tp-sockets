package py.una.client;

import javax.swing.*;

import py.una.entidad.User;

import java.net.*;
import java.io.*;
import java.awt.event.*;

public class InterfazLlamada extends JFrame {
    private static final long serialVersionUID = 10000;
    //private Client cliente;
    private PrintWriter out = null;
    private String username;    // El username de con quien se está hablando
    private Socket socket;

    /* Componentes de la interfaz gráfica */
    private JTextArea textArea;     /* Investigar su uso jejej */
    private JTextField textField;
    private JButton btnEnviar;
    private JButton btnColgar;

    public InterfazLlamada(Client cliente, Socket socket, User usuario) {
        super("Llamada en curso con " + usuario.getUsername());
        //this.cliente = cliente;

        this.username = usuario.getUsername();      /* El usuario con quien se está hablando */
        try {
            this.out = new PrintWriter(socket.getOutputStream(), true);
        } catch(IOException e) {
            System.out.println("Error al abrir flujo de salida: " + e.getMessage());
        }
        this.socket = socket;
        System.out.println("Paso 1");
        this.textArea = new JTextArea(10, 50);
        this.textField = new JTextField();
        this.btnEnviar = new JButton("Enviar");
        this.btnColgar = new JButton("Colgar");

        System.out.println("Paso 2");
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.add(this.btnColgar);
        panel.add(this.btnEnviar);

        System.out.println("Paso 3");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.btnEnviar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                onEnviarActionPerformed(evt);
            }
        });

        System.out.println("Paso 4");

        this.btnColgar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                onColgarActionPerformed(evt);
            }
        });

        System.out.println("Paso 5");

        this.setSize(400, 300);
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        this.add(this.textArea);
        this.add(this.textField);
        this.add(panel);
        this.setVisible(true);

        System.out.println("Paso 6");
    }


    public void onEnviarActionPerformed(ActionEvent event) {
        String outputLine = this.textField.getText();
        this.agregarNuevoMensaje(outputLine, false);
        this.out.println(outputLine);
    }

    public void onColgarActionPerformed(ActionEvent evt) {
        try {
            this.out.close();
            this.socket.close();
        } catch(IOException e) {
            System.out.println("Problemas al cerrar socket: " + e.getMessage());
        }
        this.setVisible(false);
    }

    public void agregarNuevoMensaje(String msg, boolean fromServer) {
        String name = fromServer ? this.username : "Yo";
        this.textArea.append(name + ": " + msg);
    }
}
