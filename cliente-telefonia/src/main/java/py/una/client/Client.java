package py.una.client;

import py.una.entidad.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.*;

import javax.swing.*;
import java.util.LinkedList;
import py.una.client.udp.*;

public class Client extends JFrame{
    private static final long serialVersionUID = 15750;
    private User usuario;
    private UDPServer udpServer;
    public InetAddress serverHost;
    public final int serverUDPPort = 9876;
    public final int serverTCPPort = 4444;

    private InterfazLlamada interfazLlamada;

    /* Panel que va a contener a todos los clientes conectados */
    private JPanel panel;

    public Client() {
        super("Cliente de telefonia");
        this.usuario = new User();
        this.udpServer = new UDPServer(this);
        this.panel = new JPanel();
        this.panel.setLayout(new BoxLayout(this.panel, BoxLayout.Y_AXIS));
        try {
            this.serverHost = InetAddress.getByName("127.0.0.1");
        } catch(UnknownHostException e) {
            System.out.println("Host no conocido: " + e.getMessage());
        }


        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                cerrarSesion(evt);
            }
        });

        this.setSize(400, 500);
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        this.add(new JLabel("Usuarios conectados"));
        this.add(this.panel);

        this.setVisible(false);
        new Inicio(this);
        this.udpServer.initServer();
    }

    public void setUserId(int id) {
        this.usuario.setId(id);
    }

    public void setUsername(String username) {
        this.usuario.setUsername(username);
    }

    public void mostrarListaConectados(LinkedList<User> users) {
        this.panel.removeAll();
        System.out.println("Actualizando lista de conectados...");
        for(User usuario : users) {
            if(usuario.getId() != this.usuario.getId()) {
                this.panel.add(new ConectadosListItem(this, usuario));
                System.out.println(usuario.getUsername());
            }
        }
        this.panel.updateUI();
    }

    public void cerrarSesion(WindowEvent evt) {
        PacketData packetToSend = new PacketData(Operation.CERRAR_SESION, 0, "", "", this.usuario.getId(), -1);
        String packetJson = PacketDataJSON.toJson(packetToSend);

        DatagramPacket packet = new DatagramPacket(packetJson.getBytes(), packetJson.length(), this.serverHost, this.serverUDPPort);

        this.udpServer.addPacketToSend(packet);

        dispose();
        System.exit(0);
    }

    public void iniciarLlamada(User userCalled) throws IOException{
        Socket socket = new Socket(this.serverHost, this.serverTCPPort);

        System.out.println("Conectado al server mediante TCP");

        String infoClient = "{'host':'"+socket.getInetAddress()+"', 'port':'"+socket.getLocalPort()+"'}";

        PacketData packetToSend = new PacketData(Operation.IDENTIFICACION, 0, "", infoClient, this.usuario.getId(), -1);

        String packetJson = PacketDataJSON.toJson(packetToSend);

        DatagramPacket packet = new DatagramPacket(packetJson.getBytes(), packetJson.length(), this.serverHost, this.serverUDPPort);

        this.udpServer.addPacketToSend(packet);

        try {
            synchronized(this) {
                this.wait();
            }
        } catch(InterruptedException e) {
            System.out.println("Error en el wait(): " + e.getMessage());
        }

        this.interfazLlamada = new InterfazLlamada(this, socket, userCalled);
        this.interfazLlamada.setVisible(true);

        FromServer fromServer = new FromServer(socket, this);

        fromServer.start();

        try {
            fromServer.join();
        } catch(InterruptedException e) {
            System.out.println("Error en el join(): " + e.getMessage());
        }
        this.interfazLlamada.setVisible(false);
        this.interfazLlamada = null;
    }

    public void contestarLlamada(User caller) throws IOException {
        Socket socket = new Socket(this.serverHost, this.serverTCPPort);

        System.out.println("Conectado al server mediante TCP");

        String infoClient = "{'host':'"+socket.getInetAddress()+"', 'port':'"+socket.getLocalPort()+"'}";

        PacketData packetToSend = new PacketData(Operation.IDENTIFICACION, 0, "", infoClient, this.usuario.getId(), -1);

        String packetJson = PacketDataJSON.toJson(packetToSend);

        DatagramPacket packet = new DatagramPacket(packetJson.getBytes(), packetJson.length(), this.serverHost, this.serverUDPPort);

        this.udpServer.addPacketToSend(packet);

        this.interfazLlamada = new InterfazLlamada(this, socket, caller);
        this.interfazLlamada.setVisible(true);

        FromServer fromServer = new FromServer(socket, this);

        fromServer.start();

        try {
            fromServer.join();
        } catch(InterruptedException e) {
            System.out.println("Error en el join(): " + e.getMessage());
        }
        this.interfazLlamada.setVisible(false);
        this.interfazLlamada = null;
    }

    public void agregarNuevoMensaje(String msg) {
        this.interfazLlamada.agregarNuevoMensaje(msg, true);
    }

    public void cerrarInterfaz() {
        this.interfazLlamada.setVisible(false);
    }

    public void enviarCallRequest(int destinoId) {
        PacketData packetToSend = new PacketData(Operation.CALL_REQUEST, 0, "ok", "", this.usuario.getId(), destinoId);

        String packetJson = PacketDataJSON.toJson(packetToSend);

        DatagramPacket packet = new DatagramPacket(packetJson.getBytes(), packetJson.length(), this.serverHost, this.serverUDPPort);

        this.udpServer.addPacketToSend(packet);
    }

    public UDPServer getUdpServer() {
        return this.udpServer;
    }
}
