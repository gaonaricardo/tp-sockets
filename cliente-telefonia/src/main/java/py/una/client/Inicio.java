package py.una.client;

import javax.swing.*;
import java.awt.event.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.net.DatagramPacket;

import py.una.entidad.*;

public class Inicio extends JFrame {
    public static final long serialVersionUID = 14798;
    private JLabel userLabel;
    private JTextField userTextField;
    private JButton btnConfirmar;
    private Client cliente;

    public Inicio(Client cliente) {
        super("Inicio");

        this.cliente = cliente;

        this.userLabel = new JLabel("Username: ");
        this.userTextField = new JTextField();
        this.btnConfirmar = new JButton("Confirmar");

        this.btnConfirmar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                onConfirmarActionPerformed(evt);
            }
        });

        JPanel panel = new JPanel();

        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

        panel.add(this.userLabel);
        panel.add(this.userTextField);

        setSize(400, 120);
        setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        add(new JLabel("Ingrese su nombre"));
        add(panel);
        add(btnConfirmar);

        setVisible(true);

    }

    public void onConfirmarActionPerformed(ActionEvent evt) {
        String username = this.userTextField.getText();
        this.cliente.setUsername(username);
        this.cliente.setTitle(username);

        try {
            PacketData packetToSend = new PacketData(Operation.NUEVO_CLIENTE, 0, "", username, 0, -1);
            String packetJson = PacketDataJSON.toJson(packetToSend);

            DatagramPacket packet = new DatagramPacket(packetJson.getBytes(), packetJson.length(), InetAddress.getByName("127.0.0.1"), 9876);

            this.cliente.getUdpServer().addPacketToSend(packet);
        } catch(UnknownHostException e) {
            System.out.println("Error: " + e.getMessage());
        }

        this.setVisible(false);
        this.cliente.setVisible(true);
    }
}

