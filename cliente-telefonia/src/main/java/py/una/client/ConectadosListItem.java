package py.una.client;

import javax.swing.*;
import py.una.entidad.User;
import java.awt.event.*;

public class ConectadosListItem extends JPanel implements ActionListener{
    private static final long serialVersionUID = 10007;
    private User usuario;
    private JButton btnLlamar;
    private JLabel userLabel;
    private Client cliente;

    public ConectadosListItem(Client cliente, User usuario) {
        this.usuario = usuario;
        this.cliente = cliente;

        this.btnLlamar = new JButton("Llamar");
        this.userLabel = new JLabel(this.usuario.getUsername());

        this.btnLlamar.addActionListener(this);

        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

        add(userLabel);
        add(btnLlamar);

        this.setVisible(true);
    }

    public int getUserId() {
        return this.usuario.getId();
    }

    public void actionPerformed(ActionEvent e) {
        this.cliente.enviarCallRequest(this.getUserId());
    }
}
