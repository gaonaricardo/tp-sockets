package py.una.client;

import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;

import py.una.entidad.*;

public class InterfazNotificacion extends JFrame {
    public static final long serialVersionUID = 15874;

    /* Infromación necesaria */
    private Client cliente;
    private User caller;

    /* Componentes de la interfaz gáfica */
    private JLabel titulo;
    private JLabel datosUsuario;
    private JButton btnRechazar;
    private JButton btnContestar;

    public InterfazNotificacion(Client cliente, User caller) {
        super("Llamada entrante");
        this.cliente = cliente;
        this.caller = caller;

        this.titulo = new JLabel("Llamada entrante");
        this.datosUsuario = new JLabel(this.caller.getUsername() + " : " + this.caller.getId());
        this.btnRechazar = new JButton("Rechazar");
        this.btnContestar = new JButton("Contestar");

        this.btnRechazar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                onRechazar(evt);
            }
        });

        this.btnContestar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                onContestar(evt);
            }
        });

        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                onWindowClosing(evt);
            }
        });

        this.setSize(400, 300);
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        this.add(this.titulo);
        this.add(this.datosUsuario);
        this.add(this.btnRechazar);
        this.add(this.btnContestar);
        this.setVisible(true);
    }

    public void onRechazar(ActionEvent evt) {
        this.setVisible(false);
    }

    public void onContestar(ActionEvent evt) {
        try {
            this.setVisible(false);
            this.cliente.contestarLlamada(this.caller);
        } catch(IOException e) {
            System.out.println("Error al contestar la llamada: " + e.getMessage());
        }
        
        //this.dispose();
    }

    public void onWindowClosing(WindowEvent evt) {
        // TODO: falta implementar windowClosing

    }
}
