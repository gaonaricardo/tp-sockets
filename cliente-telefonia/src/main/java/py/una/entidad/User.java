package py.una.entidad;

import java.lang.reflect.Type;
import java.util.LinkedList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import jdk.nashorn.internal.parser.JSONParser;

import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

public class User {
    private int id;
    private String username;

    public User(int id, String username) {
        this.id = id;
        this.username = username;
    }

    public User() {
        this.id = 0;
        this.username = "";
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public int getId() {
        return this.id;
    }

    public String getUsername() {
        return this.username;
    }

    public static LinkedList<User> toUserList(String userList) {
        Object obj = JsonParser.parseString(userList.trim());
        JsonObject jsonObject = (JsonObject) obj;

        JsonArray jsonArray = jsonObject.get("userList").getAsJsonArray();

        LinkedList<User> listaUsers = new LinkedList<User>();

        for(int i = 0; i < jsonArray.size(); i++) {
            String jsonUser = jsonArray.get(i).getAsString();
            listaUsers.add(User.toObject(jsonUser));
        }
        /*Gson gson = new Gson(); 
 
        Type userListType = new TypeToken<LinkedList<User>>(){}.getType();
 
        LinkedList<User> userArray = gson.fromJson(userList, userListType);*/ 
        return listaUsers;
    }

    public static User toObject(String json) {
        Object obj = JsonParser.parseString(json.trim());
        JsonObject jsonObject = (JsonObject) obj;

        int id = jsonObject.get("id").getAsInt();
        String username = jsonObject.get("username").getAsString();

        return new User(id, username);
    }

    public static void main(String[] args) {
        /*String userJson = "[{'username': 'Alex','id': 1}, "
                + "{'username': 'Brian','id':2}, "
                + "{'username': 'Charles','id': 3}]";
         */
        String userJson = "[{\"id\":10507,\"username\":\"ricardo\"}]";
        
        LinkedList<User> userArray = User.toUserList(userJson);

        for(User user : userArray) {
            System.out.println(user.getUsername());
        }
    }
}
