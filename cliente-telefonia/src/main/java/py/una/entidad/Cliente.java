package py.una.entidad;

import java.net.*;
import java.util.LinkedList;

import com.google.gson.JsonObject;
import com.google.gson.JsonArray;


public class Cliente{
    private int id;
    private String username;
    private InetAddress host;
    private int port;
    private Socket socket;
    
    public Cliente(int id, String username, InetAddress host, int port){
        this.id = id;
        this.username = username;
        this.port = port;
        this.host = host;
        this.socket = null;
    }


    public static String clientesToJson(LinkedList<Cliente> clientes){
        JsonArray list = new JsonArray();
        
        for(Cliente cliente : clientes) {
            JsonObject clienteJson = new JsonObject();
            clienteJson.addProperty("id", cliente.getId());
            clienteJson.addProperty("username", cliente.getUsername());
            list.add(clienteJson.toString());
        }

        return list.toString();
    }

    public int getId(){
        return this.id;
    }

    public String getUsername() {
        return this.username;
    }

    public InetAddress getHost(){
        return this.host;
    }

    public int getPort(){
        return this.port;
    }

    public Socket getSocket() {
        return this.socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }
}
