package py.una.entidad;

public class PacketData{
    private Operation operacion;
    private int estado;
    private String mensaje;
    private String contenido;
    private int origen;
    private int destino;

    public PacketData(Operation operacion, int estado, String mensaje, String contenido, int origen, int destino){
        this.operacion = operacion;
        this.estado = estado;
        this.mensaje = mensaje;
        this.contenido = contenido;
        this.origen = origen;           /* El servidor utiliza el id 1 */
        this.destino = destino;
    }

    public PacketData(Operation operacion, int estado, String mensaje, int origen){
        this(operacion, estado, mensaje, "", origen, -1);
    }

    public Operation getOperacion(){
        return this.operacion;
    }

    public int getEstado(){
        return this.estado;
    }

    public String getMensaje(){
        return this.mensaje;
    }

    public String getContenido(){
        return this.contenido;
    }

    public int getOrigen() {
        return this.origen;
    }

    public int getDestino(){ 
        return this.destino;
    }

}   