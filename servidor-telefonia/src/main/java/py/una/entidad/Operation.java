package py.una.entidad;

import java.util.Map;
import java.util.HashMap;

public enum Operation {
    NUEVO_CLIENTE(0),
    VER_CONECTADOS(1),
    INICIAR_LLAMADA(2),
    CONVERSAR(3),
    FINALIZAR_LLAMADA(4),
    CONFIRMAR_RECEPCION(5),
    CALL_REQUEST(6),
    CALL_NOTIFY(7),
    IDENTIFICACION(8),
    CERRAR_SESION(9),
    LLAMADA_CONTESTADA(10);

    private int value;
    private static final Map<Integer, Operation> intToTypeMap = new HashMap<Integer, Operation>();

    static {
        for (Operation type : Operation.values()) {
            intToTypeMap.put(type.value, type);
        }
    }

    public static Operation valueOf(int i) {
        Operation type = intToTypeMap.get(Integer.valueOf(i));
        return type;
    }

    private Operation(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}