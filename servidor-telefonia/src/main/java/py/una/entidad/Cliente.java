package py.una.entidad;

import java.net.*;
import java.util.LinkedList;

import com.google.gson.JsonObject;
import com.google.gson.JsonArray;


public class Cliente{
    private int id;
    private String username;
    private InetAddress host;
    private int port;
    private Socket socket;
    private boolean ocupado;
    
    public Cliente(int id, String username, InetAddress host, int port){
        this.id = id;
        this.username = username;
        this.port = port;
        this.host = host;
        this.socket = null;
        this.ocupado = false;
    }

    public static String clientesToJson(LinkedList<Cliente> clientes){
        JsonArray list = new JsonArray();
        JsonObject jsonObject = new JsonObject();
        
        for(Cliente cliente : clientes) {
            JsonObject clienteJson = new JsonObject();
            clienteJson.addProperty("id", cliente.getId());
            clienteJson.addProperty("username", cliente.getUsername());
            list.add(clienteJson.toString());
        }

        jsonObject.add("userList", list);

        return jsonObject.toString();
    }

    public static String toJson(Cliente cliente){
        JsonObject json = new JsonObject();
        json.addProperty("id", cliente.getId());
        json.addProperty("username", cliente.getUsername());

        return json.toString();
    }

    public int getId(){
        return this.id;
    }

    public String getUsername() {
        return this.username;
    }

    public InetAddress getHost(){
        return this.host;
    }

    public int getPort(){
        return this.port;
    }

    public Socket getSocket() {
        return this.socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public void setOcupado(boolean state) {
        this.ocupado = state;
    }

    public boolean getOcupado() {
        return this.ocupado;
    }
}
