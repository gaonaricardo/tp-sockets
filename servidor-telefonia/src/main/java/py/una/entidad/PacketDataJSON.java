package py.una.entidad;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.util.LinkedList;
import java.util.Iterator;

public class PacketDataJSON{

    public static String toJson(PacketData packet){
        JsonObject json = new JsonObject();
        json.addProperty("operacion", packet.getOperacion().getValue());
        json.addProperty("estado", packet.getEstado());
        json.addProperty("mensaje", packet.getMensaje());
        json.addProperty("contenido", packet.getContenido());
        json.addProperty("origen", packet.getOrigen());
        json.addProperty("destino", packet.getDestino());

        return json.toString();
    }

    public static PacketData toObject(String json){
        Object obj = JsonParser.parseString(json.trim());
        JsonObject jsonObject = (JsonObject) obj;

        Operation operacion = Operation.valueOf(jsonObject.get("operacion").getAsInt());
        int estado = jsonObject.get("estado").getAsInt();
        String mensaje = jsonObject.get("mensaje").getAsString();
        String contenido = jsonObject.get("contenido").getAsString();
        int origen = jsonObject.get("origen").getAsInt();
        int destino = jsonObject.get("destino").getAsInt();

        return new PacketData(operacion, estado, mensaje, contenido, origen, destino);
    }

    public static LinkedList<String> listFromJson(String json){
        LinkedList<String> listaSalida = new LinkedList<>();
        try{
            JsonObject jsonObj = new JsonObject();
            jsonObj.add("lista", JsonParser.parseString(json));

            JsonArray cliArray = (JsonArray)jsonObj.get("lista");
            Iterator<JsonElement> it = cliArray.iterator();

            while(it.hasNext()){
                listaSalida.add(it.next().getAsString());
            }

        }catch(JsonParseException e){
            e.printStackTrace();
        }
        
        return listaSalida;
    }
}