package py.una.server.udp;

import java.net.DatagramPacket;
import py.una.entidad.*;
import py.una.server.*;
import py.una.server.services.*;

public class PacketHandler extends Thread{
    /* Almacenamos datos del paquete */
    private DatagramPacket packet;
    /* Almacenamos datos del servidor */
    private Server server;

    public PacketHandler(Server server, DatagramPacket packet){
        this.packet = packet;
        this.server = server;
    }

    public void run(){
        /* Obtenemos los campos del paquete en una instancia de PacketData */
        PacketData packetData = PacketDataJSON.toObject(new String(this.packet.getData()));
        /* Obtenemos la operación */
        Operation operation = packetData.getOperacion();

        System.out.println("Un paquete ha llegado!");

        /* De acuerdo a la operación creamos un hilo para cada servicio */
        if(operation == Operation.NUEVO_CLIENTE){
            System.out.println("Un nuevo cliente se ha conectado");
            new ActivarClienteService(this.server, this.packet).ejecutar();
            new VerClientesActivosService(this.server, this.packet).ejecutar();
        }else if(operation == Operation.VER_CONECTADOS){
            new VerClientesActivosService(this.server, this.packet).ejecutar();
        }else if(operation == Operation.CALL_REQUEST) {
            new LlamadaService(this.server, this.packet).ejecutar();
        }else if(operation == Operation.IDENTIFICACION) {
            new IdentificacionService(this.server, this.packet).ejecutar();
        }else if(operation == Operation.CERRAR_SESION) {
            int id = packetData.getOrigen();

            Cliente cliente = this.server.getClienteById(id);

            this.server.getClientes().remove(cliente);
        }
    }

}