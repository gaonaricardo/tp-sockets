package py.una.server.udp;

import java.io.IOException;
import java.net.*;
import java.util.LinkedList;

import py.una.server.Server;


public class UDPServer {
    /* Socket para el envío de datagramas */
    private DatagramSocket socket;
    /* Lista de paquetes pendientes de envío */
    private LinkedList<DatagramPacket> enviosPendientes;
    /* Necesitamos mantener una instancia del servidor como atributo */
    private Server server;

    /* Hilos para enviar y recibir paquetes UDP */
    private UDPSender sender;
    private UDPReceiver receiver;

    public UDPServer(Server server, int port) {
        try {
            this.socket = new DatagramSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.server = server;
        this.enviosPendientes = new LinkedList<>();
        this.sender = new UDPSender(this);
        this.receiver = new UDPReceiver(this);
    }

    public void initServer() {
        try {
            this.sender.start();
            this.receiver.start();
            System.out.println("Servidor UDP iniciado!");
            System.out.println("Puerto utilizado: " + this.socket.getLocalPort());

            this.sender.join();
            this.receiver.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public UDPSender getSender() {
        return this.sender;
    }

    public UDPReceiver getReceiver() {
        return this.receiver;
    }

    /* Agregar paquetes para enviar */
    public void addPacketToSend(DatagramPacket packet) {

        try {
            /* Nos aseguramos de que solo un hilo a la vez acceda */
            synchronized(this.enviosPendientes){
                this.enviosPendientes.add(packet);
                this.enviosPendientes.notify();
                System.out.println("Se agrego un paquete a la cola de envios");
            }
        } catch(IllegalMonitorStateException e) {
            System.out.println("Error en el notify(): " + e.getMessage());
        }
    }

    /* Obtener un paquete para enviar de la lista */
    public DatagramPacket getPacketToSend() {
        DatagramPacket packet = null;

        try {
            /* Nos aseguramos de que solo un hilo a la vez acceda */
            synchronized(this.enviosPendientes) {
                if(this.enviosPendientes.size() == 0)
                    this.enviosPendientes.wait();
                
                packet = this.enviosPendientes.removeLast();
                System.out.println("Se obtiene un paquete para enviar!");
            }
        } catch (InterruptedException e1) {
            System.out.println("Error en el wait(): " + e1.getMessage());
        }

        return packet;
    }

    public DatagramSocket getSocket(){
        return this.socket;
    }

    public Server getServer(){
        return this.server;
    }

}  

