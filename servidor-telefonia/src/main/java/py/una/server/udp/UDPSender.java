package py.una.server.udp;

import java.io.*;
import java.net.DatagramPacket;;

public class UDPSender extends Thread{

    private UDPServer udpServer;

    public UDPSender(UDPServer udpServer){
        this.udpServer = udpServer;
    }

    public void run(){
        DatagramPacket packet;
        while(true){
            try {
                packet = this.udpServer.getPacketToSend();
                this.udpServer.getSocket().send(packet);
                System.out.println("Paquete enviado!");
            } catch(IOException e) {
                System.out.println("Error al enviar el paquete: " + e.getMessage());
            }
        }
    }
}



/*

try {
                this.udpServer.getSem().acquire();
                tam = this.udpServer.getBuzonSalida().size();
                this.udpServer.getSem().release();
            } catch(InterruptedException e) {
                e.printStackTrace();
            }

            if(tam > 0){
                try{
                    this.udpServer.getSem().acquire();
                    this.udpServer.getSocket().send(this.udpServer.getBuzonSalida().getLast());
                    this.udpServer.getBuzonSalida().removeLast();
                    this.udpServer.getSem().release();

                    System.out.println("Mensaje pendiente enviado!");
                }catch(InterruptedException e){
                    e.printStackTrace();
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
*/