package py.una.server.udp;

import java.net.*;

public class UDPReceiver extends Thread{
    /* Almacenamos la instancia del servidor udp para poder acceder a sus métodos */
    private UDPServer udpServer;

    public UDPReceiver(UDPServer udpServer){
        this.udpServer = udpServer;
    }
    
    public void run() {
        byte[] receiveData;
        try{
            while (true) {
                /* Arreglo para los datos del paquete entrante */
                receiveData = new byte[1024];
                /* Instancia de DatagramPacket para almacenar el paquete entrante */
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                /* Accedemos al socket del servidor udp */
                this.udpServer.getSocket().receive(receivePacket);
                /* Creamos un hilo para el packet handler */
                new PacketHandler(this.udpServer.getServer(), receivePacket).start();
            }
        }catch(Exception e){
            e.printStackTrace();
            System.exit(1);
        }
    }
}