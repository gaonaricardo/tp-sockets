package py.una.server.services;

import py.una.server.*;
import java.net.*;
import py.una.entidad.*;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class IdentificacionService{
    /* Almacenamos datos del paquete */
    private DatagramPacket packet;
    /* Almacenamos datos del server */
    private Server server;

    public IdentificacionService(Server server, DatagramPacket packet) {
        this.packet = packet;
        this.server = server;
    }

    private class HostPort {
        private String host;
        private int port;

        public int getPort() { return this.port; }
        public String getHost() { return this.host; }

        public void toObject(String json) {
            Object obj = JsonParser.parseString(json.trim());
            JsonObject jsonObject = (JsonObject) obj;
            
            this.host = jsonObject.get("host").getAsString();
            this.port = jsonObject.get("port").getAsInt();
        }
    }

    public void ejecutar() {
        try {
            /* Obtenemos los datos del paquete */
            PacketData receiveData = PacketDataJSON.toObject(new String(packet.getData()));
            /* Obtenemos el puerto y la direccion ip */
            HostPort hp = new HostPort();
            hp.toObject(receiveData.getContenido());
            /* Obtenemos el socket correspondiente al cliente */
            System.out.println(hp.getPort() + ": " + hp.getHost());
            Socket clientSocket = this.server.getSocketByPort(hp.getHost(), hp.getPort());
            /* Obtenemos la instancia del cliente */
            Cliente cliente = this.server.getClienteById(receiveData.getOrigen());
            /* Asociamos el socket obtenido al cliente */
            if(clientSocket == null) System.out.println("(IdentificacionService)No se pudo obtener el socket del cliente");
            cliente.setSocket(clientSocket);
            /* Eliminamos de la lista el socket obtenido */
            this.server.removeConnection(clientSocket);

            try {
                synchronized(cliente) {
                    /* Notificamos para desbloquear al hilo que llamó al método wait() del cliente */
                    cliente.notify();
                }
            } catch(IllegalMonitorStateException e) {
                System.out.println("(IdentificacionService)Error en el notify: " + e.getMessage());
            }

        } catch(Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

}
