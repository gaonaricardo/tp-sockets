package py.una.server.services;

import py.una.entidad.*;
import java.net.DatagramPacket;
import py.una.server.Server;

public class VerClientesActivosService{
    private Server server;
    private DatagramPacket packet;

    public VerClientesActivosService(Server server, DatagramPacket packet) {
        this.server = server;
        this.packet = packet;
    }

    public void ejecutar() {
        try {
            PacketData sendData;
            synchronized(this.server.getClientes()){
                String conectados = Cliente.clientesToJson(this.server.getClientes());
                sendData = new PacketData(Operation.VER_CONECTADOS, 0, "ok", conectados, 1, -1);
                System.out.println(conectados);
            }                   
            
            String dataJson = PacketDataJSON.toJson(sendData);  
            
            for(Cliente cliente : this.server.getClientes()) {
                DatagramPacket packetToSend = new DatagramPacket(dataJson.getBytes(), dataJson.getBytes().length,cliente.getHost(), cliente.getPort());
                this.server.getUDPServer().addPacketToSend(packetToSend);  
            }
    
                        
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }
}
