package py.una.server.services;
import py.una.entidad.*;
import py.una.server.Server;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.net.Socket;

import java.io.*;

public class LlamadaService{
    /* Almacenamos datos del server */
    private Server server;
    /* Almacenamos datos del paquete */
    private DatagramPacket packet;
    /* Tiempo de espera a la conexion de un cliente */
    private final int llamadaTimeout = 30000;

    private class LlamadaEndPoint extends Thread {
        /* Necesitamos los sockets del origen y del destino */
        private Socket socketA;
        private Socket socketB;

        public LlamadaEndPoint(Cliente clienteA, Cliente clienteB) {
            /* Obtenemos los sockets de las instancias de los clientes */
            this.socketA = clienteA.getSocket();
            this.socketB = clienteB.getSocket();

            if(this.socketA == null) System.out.println("No se encuentra el socket de A");
            if(this.socketB == null) System.out.println("No se encuentra el socket de B");
        }

        public void run() {
            BufferedReader inA;
            PrintWriter outB;
            /* Variable para almacenar el flujo proveniente de A */
            String inputLineA;
            /* Variable utilizada como bandera para determinar el fin de una llamada */
            boolean endPhoneCall = false; 

            try {
                /* BufferedReader para el flujo proveniente de A */
                inA = new BufferedReader(new InputStreamReader(this.socketA.getInputStream()));
                /* PrintWriter para enviar el flujo al cliente B */
                outB = new PrintWriter(this.socketB.getOutputStream(), true);
                /* Mientras no se alcance la condición de fin de llamada (endPhoneCall = true) */
                while(!endPhoneCall) {
                    try {
                        /* Leemos el flujo proveniente del cliente A */
                        if((inputLineA = inA.readLine()) != null)
                            /* Enviamos el flujo al cliente B */
                            outB.println(inputLineA);
                        
                        if(this.socketA.isClosed() || this.socketB.isClosed()) endPhoneCall = true;
                    } catch(SocketException e) {
                        /* El cierre de un socket provoca esta excepción */
                        endPhoneCall = true;
                        this.socketA.close();
                        this.socketB.close();
                    }
                }

                System.out.println("Fin de la llamada");

            } catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    public LlamadaService(Server server, DatagramPacket packet) {
        this.server = server;
        this.packet = packet;
    }

    public void ejecutar() {
        /*
            ClienteA: hace la llamada
            ClienteB: recibe la llamada
        */
        try {
            /* Recuperamos los datos del paquete en formato json */
            String packetDataJson = new String(packet.getData());
            /* Creamos una instancia de PacketData con los datos del paquete */
            PacketData packetData = PacketDataJSON.toObject(packetDataJson);

            /* Obtenemos el id del cliente A y del cliente B */
            int idClienteA = packetData.getOrigen();
            int idClienteB = packetData.getDestino();

            /* Obtenemos las instancias de ambos clientes mediante sus respectivos id's */
            Cliente clienteA = this.server.getClienteById(idClienteA);
            Cliente clienteB = this.server.getClienteById(idClienteB);

            if(clienteA == null) System.out.println("ClienteA no encontrado");
            if(clienteB == null) System.out.println("ClienteB no encontrado");

            // Si el cliente B se encuentra disponible
            if(!clienteB.getOcupado()) {
                /* Se le avisa al cliente A que puede conectarse vía TCP */
                String userCalled = Cliente.toJson(clienteB);
                PacketData respuesta = new PacketData(Operation.INICIAR_LLAMADA, 0, "", userCalled, 1, -1);
                String respuestaJson = PacketDataJSON.toJson(respuesta);
                DatagramPacket responsePacket = new DatagramPacket(respuestaJson.getBytes(), respuestaJson.getBytes().length, this.packet.getAddress(), this.packet.getPort());
                this.server.getUDPServer().addPacketToSend(responsePacket);

                /* Debemos esperar a que el cliente A se conecte */
                try {
                    synchronized(clienteA) {
                        clienteA.wait(this.llamadaTimeout);
                    }
                } catch(InterruptedException e) {
                    System.out.println("Error al esperar al cliente A: " + e.getMessage());
                }
                /* Falta verificar si se seteó el socket */
                clienteA.setOcupado(true);

                /* Se le notifica al cliente B de la llamada entrante */
                String userCaller = Cliente.toJson(clienteA);
                respuesta = new PacketData(Operation.CALL_NOTIFY, 0, "",userCaller, 1, -1);
                respuestaJson = PacketDataJSON.toJson(respuesta);
                responsePacket = new DatagramPacket(respuestaJson.getBytes(), respuestaJson.getBytes().length, clienteB.getHost(), clienteB.getPort());
                this.server.getUDPServer().addPacketToSend(responsePacket);

                /* Esperamos a que el cliente B se conecte */
                try {
                    synchronized(clienteB) {
                        clienteB.wait(this.llamadaTimeout);
                    }
                } catch(InterruptedException e) {
                    System.out.println("Error al esperar al cliente B: " + e.getMessage());
                }
                /* Falta verificar si se seteó el socket */
                clienteB.setOcupado(true);

                respuesta = new PacketData(Operation.LLAMADA_CONTESTADA, 0, "", 1);
                respuestaJson = PacketDataJSON.toJson(respuesta);
                responsePacket = new DatagramPacket(respuestaJson.getBytes(), respuestaJson.getBytes().length, this.packet.getAddress(), this.packet.getPort());
                this.server.getUDPServer().addPacketToSend(responsePacket);

                /* Hilo para enviar el flujo de A hacia B */
                LlamadaEndPoint origenEndPoint = new LlamadaEndPoint(clienteA, clienteB);
                /* Hilo para enviar el flujo de B hacia A */
                LlamadaEndPoint destinoEndPoint = new LlamadaEndPoint(clienteB, clienteA);

                /* Iniciamos ambos hilos endpoints */
                origenEndPoint.start();
                destinoEndPoint.start();

                /* Esperamos a que finalice la llamada */
                origenEndPoint.join();
                destinoEndPoint.join();

                clienteA.setOcupado(false);
                clienteB.setOcupado(false);

            }
        } catch(Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
