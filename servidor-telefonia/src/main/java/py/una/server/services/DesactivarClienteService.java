package py.una.server.services;

import py.una.server.Server;
import java.net.DatagramPacket;

public class DesactivarClienteService{
    
    private Server server;
    private DatagramPacket packet;

    public DesactivarClienteService(Server server, DatagramPacket packet) {
        this.server = server;
        this.packet = packet;
    }

    public void ejecutar() {
        server.toString();
        packet.toString();
    }
}