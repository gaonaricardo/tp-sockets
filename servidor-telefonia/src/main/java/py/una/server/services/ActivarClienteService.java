package py.una.server.services;

import py.una.entidad.*;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Random;
import py.una.server.Server;

public class ActivarClienteService{
    private DatagramPacket packet;
    private Server server;

    public ActivarClienteService(Server server, DatagramPacket packet) {
        this.packet = packet;
        this.server = server;
    }

    public void ejecutar() {
        try {
            PacketData receiveData = PacketDataJSON.toObject(new String(packet.getData()));                    
                      
            String username = receiveData.getContenido();
            InetAddress host = this.packet.getAddress();
            int id = this.generarID();

            this.server.addNewClient(new Cliente(id, username, host, packet.getPort()));

            PacketData sendData = new PacketData(Operation.CONFIRMAR_RECEPCION, 0, "ok", Integer.toString(id), 1, -1);
            String dataJson = PacketDataJSON.toJson(sendData); 

            DatagramPacket packetToSend = new DatagramPacket(dataJson.getBytes(), dataJson.getBytes().length,packet.getAddress(), packet.getPort());
            this.server.getUDPServer().addPacketToSend(packetToSend);         
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }

    public int generarID() {
        Random generador = new Random();
        int id = 0;
        boolean finish = false;

        while(!finish) {
            finish = true;
            id = generador.nextInt() % 89999;
            id = id > 0 ? id : -1 * id;
            id += 10001;
            finish = !this.server.existId(id);
        }

        return id;
    }

}
