package py.una.server.tcp;

import java.net.*;
import java.io.*;
import py.una.server.Server;

public class TCPServer extends Thread{

    private Server server;
    private int port;
    private ServerSocket serverSocket;

    public TCPServer(Server server, int port) {
        this.server = server;
        this.port = port;

        try {
            this.serverSocket = new ServerSocket(this.port);
        } catch (IOException e) {
            System.err.println("No se puede abrir el puerto: " + this.port + ".");
            System.exit(1);
        }
        System.out.println("Servidor TCP iniciado!");
        System.out.println("Puerto abierto: " + this.port + ".");
    }

    public void run() {
        try {
            while(true) {
                Socket clientSocket = serverSocket.accept();
                this.server.addNewConnection(clientSocket);
            }
        } catch (IOException e) {
            System.err.println("Fallo el accept().");
            System.exit(1);
        }

    }
    
}
