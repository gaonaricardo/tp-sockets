package py.una.server;

import py.una.entidad.*;
import java.util.LinkedList;
import py.una.server.udp.*;
import py.una.server.tcp.*;
import java.util.concurrent.Semaphore;
import java.net.*;

public class Server{
    /* Lista de clientes activos */
    private LinkedList<Cliente> clientes;
    /* Lista de sockets TCP de clientes */
    private LinkedList<Socket> conexiones;
    /* Servidor UDP para envío y recepción de paquetes */
    private UDPServer udpServer;
    /* Servidor TCP para llamadas telefónicas */
    private TCPServer tcpServer;
    
    /* Definimos los puertos donde estarán ubicados los servidores TCP y UDP */
    private int portUDP = 9876;
    private int portTCP = 4444;

    public Server(){
        /* Creamos la lista enlazada para almacenar los clientes conectados */
        this.clientes = new LinkedList<>();
        /* Instanciamos el servidor UDP */
        this.udpServer = new UDPServer(this, portUDP);
        /* Instanciamos el servidor TCP */
        this.tcpServer = new TCPServer(this, portTCP);
        /* Creamos la lista para conexiones TCP */
        this.conexiones = new LinkedList<Socket>();
    }

    public void initServer(){
        /* Arrancamos el servidor TCP en otro hilo de ejecución */
        this.tcpServer.start();
        /* Arrancamos el servidor UDP */
        this.udpServer.initServer();

        try {
            this.tcpServer.join();
        } catch(InterruptedException e) {
            System.out.println("Error en el join(): " + e.getMessage());
        }
    }
    
    
    public LinkedList<Cliente> getClientes(){
        /* Obtenemos todos los clientes conectados */
        return this.clientes;
    }
    
    /* Verificamos si ya existe un usuario con el id pasado como parámetro */
    public synchronized boolean existId(int id) {
        for (Cliente cliente : this.clientes) {
            if(cliente.getId() == id) 
                return true;
        }
        return false;
    }

    /* Obtenemos la instancia de un cliente dado su id */
    public synchronized Cliente getClienteById(int id) {
        for(Cliente c : this.clientes) {
            if(c.getId() == id)
                return c;
        }
        return null;
    }

    /* Agregamos un nuevo cliente a la lista de clientes conectados */
    public synchronized void addNewClient(Cliente cliente) {
        this.clientes.add(cliente);
    }

    /* Obtenemos la instancia del servidor UDP */
    public UDPServer getUDPServer() {
        return this.udpServer;
    }

    /* Agregamos un nuevo socket a la lista de sockets TCP */
    public synchronized void addNewConnection(Socket socket) {
        this.conexiones.add(socket);
        System.out.println("Nuevo socket agregado (Host:127.0.0.1, Port:"+socket.getPort()+")");
    }

    public synchronized Socket getSocketByPort(String host, int port) {
        //TODO: Obtenemos un socket dado el puerto y la direccion ip 
        for(Socket socket: conexiones) {
            //try {
                if(socket.getPort() == port /*&& socket.getInetAddress() == InetAddress.getByName(host)*/) {
                    return socket;
                }
            /*} catch(UnknownHostException e) {
                System.out.println("Error: " + e.getMessage());
            }*/
        }
        return null;
    }

    public synchronized void removeConnection(Socket socket) {
        this.conexiones.remove(socket);
    }

}
